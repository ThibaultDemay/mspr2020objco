<#import "/spring.ftl" as spring/>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitionnal//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
    <#include "header.ftl">
</head>
<body>
<div class="wrapper">
    <#include "sidebar.ftl">
    <div id="content">
        <div class="container">
            <div class="card border-0 shadow my-5">
                <div class="card-body p-5">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <h2>La liste des centres de traitement</h2>
                            <p><a href="${'/recyclApplication/index'}">Retourner à l'index</a></p>
                        </div>
                        <div class="panel-body">

                            <table class="table table-striped">
                                <thead>
                                <tr>
                                    <th>NoCentre</th>
                                    <th>NomCentre</th>
                                    <th>NoRueCentre</th>
                                    <th>RueCentre</th>
                                    <th>CpostalCentre</th>
                                    <th>VilleCentre</th>
                                </tr>
                                </thead>
                                <tbody>
                                <#list centres as centre>
                                    <tr>
                                        <td>
                                            <a href="${'/recyclApplication/centres-traitement/' + centre.no}"><#if centre.nom??>${centre.no}<#else></#if>
                                        </td>
                                        <td><#if centre.nom??>${centre.nom}<#else></#if></td>
                                        <td><#if centre.nom??>${centre.noRue}<#else></#if></td>
                                        <td><#if centre.nom??>${centre.rue}<#else></#if></td>
                                        <td><#if centre.nom??>${centre.cPostal}<#else></#if></td>
                                        <td><#if centre.nom??>${centre.ville}<#else></#if></td>
                                    </tr>
                                </#list>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

</body>
</html>