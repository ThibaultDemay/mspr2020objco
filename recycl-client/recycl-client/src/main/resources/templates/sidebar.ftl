<body>

<!-- Sidebar  -->
<nav id="sidebar">
    <div class="sidebar-header">
        <h3>Recycl Monitoring</h3>
        <strong>RM</strong>
    </div>

    <ul class="list-unstyled components">
        <li class="active">
            <a href="#principalSubMenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                <i class="fas fa-home"></i>
                Monitoring Principal
            </a>
            <ul class="collapse list-unstyled" id="principalSubMenu">
                <li><a href="${'camions'}">Listing des camions</a></li>
                <li><a href="${'centres-traitement'}">Listing des centres de traitement</a></li>
                <li><a href="${'types-dechets'}">Listing des types de dechet</a></li>
                <li><a href="${'employes'}">Annuaire des employés</a></li>
                <li><a href="${'tournees'}">Tournees programmées</a></li>
                <li><a href="${'entreprises'}">Entreprises clientes</a></li>
                <li><a href="${'demandes'}">Demandes réalisées</a></li>
            </ul>
        </li>

    </ul>


    <ul class="list-unstyled components">
        <li class="active">
            <a href="#annexesFunctionSubMenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                <i class="fas fa-home"></i>
               Fonctions Annexes
            </a>
            <ul class="collapse list-unstyled" id="annexesFunctionSubMenu">
                <li><a href="${'demandesDates'}">Demandes sur une période</a></li>
                <li><a href="${'demandesSansTournee'}">Demandes qui ne sont pas dans une tournee</a></li>
                <li>
                    <a href="${'employes-inf-tournee'}">Les employés ayant fait moins de N
                        tournées</a></li>
                <li><a href="${'entreprisesNombreDemandes'}">Recherche entreprise par comparaison du nombre de demande</a></li>
                <li><a href="${'quantite-dechets-centre'}">Inventaire des quantités de déchets collectées </a>
            </ul>
        </li>

    </ul>

</nav>


<script>
    $(document).ready(function () {

        $('#sidebarCollapse').on('click', function () {
            $('#sidebar').toggleClass('active');
        });
    });
</script>

</body>

