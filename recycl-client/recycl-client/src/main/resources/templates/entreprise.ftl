<#import "/spring.ftl" as spring/>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitionnal//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
    <#include "header.ftl">
</head>
<body>
<div class="wrapper">
    <#include "sidebar.ftl">
    <div id="content">
        <div class="container">
            <div class="card border-0 shadow my-5">
                <div class="card-body p-5">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <h2>Détails sur l'entreprise : ${entreprise.siret}</h2>
                        </div>
                        <div class="panel-body">

                            <p>Raison Sociale : ${entreprise.raisonSociale}</p>
                            <h3>Adresse :</h3>
                            <p>Numéro : ${entreprise.noRue}</p>
                            <p>Rue : ${entreprise.rue}</p>
                            <p>Code Postal : ${entreprise.cPostal}</p>
                            <p>Ville : ${entreprise.ville}</p>
                            <h3>Coordonnées :</h3>
                            <p>Téléphone : ${entreprise.noTel}</p>
                            <p>Contact : ${entreprise.contact}</p>


                        </div>
                    </div>


                    <p><a href="${'/recyclApplication/entreprises/'}">Retourner à la liste des entreprises</a></p>

                </div>
            </div>
        </div>
    </div>
</div>

</body>
</html>