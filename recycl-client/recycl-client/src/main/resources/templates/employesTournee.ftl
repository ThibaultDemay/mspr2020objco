<#import "/spring.ftl" as spring/>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitionnal//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
    <#include "header.ftl">
</head>
<body>
<div class="wrapper">
    <#include "sidebar.ftl">
    <div id="content">
        <div class="container">
            <div class="card border-0 shadow my-5">
                <div class="card-body p-5">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <h2>La liste des employés ayant réalisé moins de ${nbTournees} tournées</h2>
                            <p><a href="${'/recyclApplication/index'}">Retourner à l'index</a></p>
                        </div>
                        <div class="panel-body">

                            <table class="table table-striped">
                                <thead>
                                <tr>
                                    <th>NoEmploye</th>
                                    <th>Nom</th>
                                    <th>Prenom</th>
                                    <th>Nombre de tournée(s)</th>
                                </tr>
                                </thead>
                                <tbody>
                                <#list employes as employe>
                                    <tr>
                                        <td><a href="${'/recyclApplication/employes/' + employe.no}">${employe.no}</a>
                                        </td>
                                        <td><#if employe.nom??>${employe.nom}<#else></#if></td>
                                        <td><#if employe.prenom??>${employe.prenom}<#else></#if></td>
                                        <td><#if employe.nbTourneeTotal??>${employe.nbTourneeTotal}<#else></#if></td>
                                    </tr>
                                </#list>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

</body>
</html>