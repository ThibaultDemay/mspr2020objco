<#import "/spring.ftl" as spring/>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitionnal//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
    <#include "header.ftl">
</head>
<body>
<div class="wrapper">
    <#include "sidebar.ftl">
    <div id="content">
        <div class="container">
            <div class="card border-0 shadow my-5">
                <div class="card-body p-5">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <h2>La liste des entreprises</h2>
                            <p><a href="${'/recyclApplication/entreprisesNombreDemandes'}">Nouvelle recherche</a></p>
                            <p><a href="${'/recyclApplication/index'}">Retourner à l'index</a></p>
                        </div>
                        <div class="panel-body">

                            <table class="table table-striped">
                                <thead>
                                <tr>
                                    <th>Raison sociale</th>
                                    <th>Nombre de tournees</th>
                                </tr>
                                </thead>
                                <tbody>
                                <#list entreprises as entreprise>
                                    <tr>
                                        <td><#if entreprise.raisonSociale??>${entreprise.raisonSociale}<#else></#if></a></td>
                                        <td><#if entreprise.nombreDemandes??>${entreprise.nombreDemandes}<#else></#if></td>

                                    </tr>
                                </#list>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

</body>
</html>