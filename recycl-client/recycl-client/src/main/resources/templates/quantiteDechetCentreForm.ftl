<#import "/spring.ftl" as spring/>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>

<head>
    <#include "header.ftl">
</head>
<body>
<div class="wrapper">
    <#include "sidebar.ftl">
    <div id="content">
        <div class="container">
            <div class="card border-0 shadow my-5">
                <div class="card-body p-5">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <h2>Inventaire des quantités de déchets collectées </h2>
                            <h3>Par types, par centres et/ou par période </h3>

                            <p><a href="${'/recyclApplication/index'}">Retourner à l'index</a></p>
                        </div>
                        <div class="panel-body">

                            <form action="" method="post">

                                <div class="form-group">
                                    <label for="noCentreLabel">Centre de traitements</label>

                                    <select class="custom-select mr-sm-2" name="noCentre" id="noCentre" required>
                                        <option value="">Choisissez votre centre</option>

                                        <#list centresTraitement as centre>

                                            <option value="<#if centre.nom??>${centre.no}">Centre n°${centre.no}
                                                : ${centre.nom}<#else></#if></option>


                                        </#list>
                                    </select>


                                    <#--                    <input type="text" class="form-control" name="noCentre" id="noCentre"-->
                                    <#--                           placeholder="Numéro du centre">-->
                                    <label for="checkAllCentresLabel">Recherche Nationale ?</label>
                                    <input type="checkbox"
                                           onchange="
                           document.getElementById('noCentre').disabled = this.checked;
                           document.getElementById('noCentre').required = !this.checked;
                           " name='checkAllCentres'
                                           id='checkAllCentres'/>
                                </div>


                                <div class="form-group">
                                    <label for="nomTypeDechetLabel">Type de déchet :</label>
                                    <select class="custom-select mr-sm-2" name="nomTypeDechet" id="nomTypeDechet"
                                            required>
                                        <option value="">Type de déchet...</option>

                                        <#list dechetsTypes as type>

                                            <option value="<#if type.nom??>${type.nom}">${type.nom}<#else></#if></option>


                                        </#list>

                                    </select>
                                    <label for="checkAllTypesDechetsLabel">Tous les types de déchets ?</label>
                                    <input type="checkbox"
                                           onchange="
                           document.getElementById('nomTypeDechet').disabled = this.checked;
                           document.getElementById('nomTypeDechet').required = !this.checked;
                           " name='checkAllTypesDechets'
                                           id='checkAllTypesDechets'/>
                                </div>

                                <div class="form-group">
                                    <label for="date1Label">Date de début</label>

                                    <input type="date" class="form-control" id="date1" name="date1" min="2017-01-01"
                                           max="2020-12-31"
                                           required>
<#--                                    <small id="date1Help" class="form-text text-muted">Formalisme 2020-12-31</small>-->
                                </div>


                                <div class="form-group">
                                    <label for="date2Label">Date de fin</label>
                                    <input type="date" class="form-control" id="date2" name="date2" min="2017-01-01"
                                           max="2020-12-31"
                                           required>
<#--                                    <small id="date2Help" class="form-text text-muted"></small>-->
                                </div>


                                <button type="submit" class="btn btn-primary">Rechercher !</button>


                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

</body>
</html>