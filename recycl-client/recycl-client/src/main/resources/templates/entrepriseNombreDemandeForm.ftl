<#import "/spring.ftl" as spring/>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>

<head>
    <#include "header.ftl">
</head>
<body>
<div class="wrapper">
    <#include "sidebar.ftl">
    <div id="content">
        <div class="container">
            <div class="card border-0 shadow my-5">
                <div class="card-body p-5">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <h2>Recherche des entreprises par nombre de demandes </h2>
                            <p><a href="${'/recyclApplication/index'}">Retourner à l'index</a></p>
                        </div>
                        <div class="panel-body">

                            <form action="" method="post">

                                <div class="form-group">

                                    <label class="form-check-label" for="exampleRadios1">
                                        Entreprise de référence (minimum de demandes) :
                                    </label>
                                    <select class="custom-select mr-sm-2" name="raisonSocialeEntrepriseSelect"
                                            id="raisonSocialeEntrepriseSelect" required>
                                        <option value="">Nom de l'entreprise</option>
                                        <#list entreprises as entreprise>
                                            <option value="<#if entreprise.raisonSociale??>${entreprise.raisonSociale}">${entreprise.raisonSociale}<#else></#if></option>
                                        </#list>
                                    </select>
                                </div>


                                <button type="submit" class="btn btn-primary">Rechercher !</button>


                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

</body>
</html>