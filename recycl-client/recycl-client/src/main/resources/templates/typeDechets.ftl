<#import "/spring.ftl" as spring/>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitionnal//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
    <#include "header.ftl">
</head>
<body>
<div class="wrapper">
    <#include "sidebar.ftl">
    <div id="content">
        <div class="card border-0 shadow my-5">
            <div class="card-body p-5">
                <div class="container">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <h2>Détails sur le type de déchet : ${typeDechets.no}</h2>
                        </div>
                        <div class="panel-body">

                            <p>N° : ${typeDechets.no} </p>
                            <p>Nom : ${typeDechets.nom}</p>
                            <p>Niveau de danger : ${typeDechets.nivDanger}</p>

                        </div>
                    </div>


                    <p><a href="${'./'}">Retourner à la liste des tournees</a></p>


                </div>
            </div>
        </div>
    </div>
</div>


</body>
</html>