<#import "/spring.ftl" as spring/>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>

<head>
    <#include "header.ftl">
</head>
<body>
<div class="wrapper">
    <#include "sidebar.ftl">
    <div id="content">
        <div class="container">
            <div class="card border-0 shadow my-5">
                <div class="card-body p-5">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <h2>Recherche des demandes par période </h2>
                            <p><a href="${'/recyclApplication/index'}">Retourner à l'index</a></p>
                        </div>
                        <div class="panel-body">

                            <form action="" method="post">


                                <div class="form-group">
                                    <label for="exampleInputEmail1">Date de début</label>

                                    <input type="date" class="form-control" id="date1" name="date1" min="2017-01-01"
                                           max="2020-12-31"
                                           placeholder="20XX-MM-JJ" required>
                                </div>


                                <div class="form-group">
                                    <label for="exampleInputEmail1">Date de fin</label>
                                    <input type="date" class="form-control" id="date2" name="date2" min="2017-01-01"
                                           max="2020-12-31"
                                           placeholder="20XX-MM-JJ" required>
                                </div>


                                <button type="submit" class="btn btn-primary">Rechercher !</button>


                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

</body>
</html>