<#import "/spring.ftl" as spring/>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitionnal//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
    <#include "header.ftl">
</head>
<body>
<div class="wrapper">
    <#include "sidebar.ftl">
    <div id="content">
        <div class="container">
            <div class="card border-0 shadow my-5">
                <div class="card-body p-5">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <h2>Détails sur le employe : ${employe.no}</h2>
                        </div>
                        <div class="panel-body">


                            <p>NoEmploye : ${employe.no}</p>
                            <p>Nom : ${employe.nom}</p>
                            <p>Prenom: ${employe.prenom}</p>
                            <#--            <p>Date de naissance : ${employe.dateNaiss?date}</p>-->
                            <#--            <p>Date d'embauche : ${employe.dateEmbauche?date}</p>-->
                            <#--            <p>Salaire : ${employe.salaire}</p>-->
                            <p>Fonction : ${employe.fonction}</p>

                        </div>
                    </div>

                    <p><a href="${'./'}">Retourner à la liste des employes</a></p>


                </div>
            </div>
        </div>
    </div>
</div>

</body>
</html>