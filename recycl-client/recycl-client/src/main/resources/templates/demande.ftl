<#import "/spring.ftl" as spring/>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitionnal//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
    <#include "header.ftl">
</head>
<body>
<div class="wrapper">
    <#include "sidebar.ftl">
    <div id="content">
        <div class="container">
            <div class="card border-0 shadow my-5">
                <div class="card-body p-5">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <h2>Détails sur la demande : ${demande.no}</h2>
                        </div>
                        <div class="panel-body">

                            <p>Demande : ${demande.no}</p>
                            <p>Date : ${demande.date?datetime}</p>
                            <p>Date d'enlèvement : ${demande.dateEnlevement?datetime}</p>
                            <p>Web_O_N : ${demande.web_O_N}</p>
                            <p>Siret de l'entreprise : ${demande.siret}</p>
                            <p>Nom de l'entreprise : ${raisonSociale}</p>
                            <p>Tournée n° : <a
                                        href="${'/recyclApplication/tournees/' + demande.noTournee}">${demande.noTournee}</a>
                            </p>

                        </div>
                    </div>
                    <h3>Détails des enlèvements associés à la demande :</h3>
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>Type de déchet</th>
                            <th>Quantité enlevée</th>
                            <th>Remarque</th>
                        </tr>
                        </thead>
                        <tbody>
                        <#list detailsdemandeForDisplay as detail>
                            <tr>
                                <td><#if detail.nomTypeDechet??>${detail.nomTypeDechet}<#else></#if></td>
                                <td><#if detail.quantiteEnlevee??>${detail.quantiteEnlevee}<#else></#if></td>
                                <td><#if detail.remarque??>${detail.remarque}<#else></#if></td>
                            </tr>
                        </#list>
                        </tbody>
                    </table>

                    <div>

                    </div>
                    <p><a href="${'./'}">Retourner à la liste des demandes</a></p>

                </div>
            </div>
        </div>
    </div>
</div>

</body>
</html>