<#import "/spring.ftl" as spring/>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitionnal//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
    <#include "header.ftl">
</head>
<body>
<div class="wrapper">
    <#include "sidebar.ftl">
    <div id="content">
        <div class="container">
            <div class="card border-0 shadow my-5">
                <div class="card-body p-5">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <h2>La liste des camions</h2>
                            <p><a href="${'/recyclApplication/index'}">Retourner à l'index</a></p>
                        </div>
                        <div class="panel-body">

                            <table class="table table-striped">
                                <thead>
                                <tr>
                                    <th>N° d'immatriculation</th>
                                    <th>Modele</th>
                                    <th>Marque</th>
                                    <th>Date d'achat</th>
                                </tr>
                                </thead>
                                <tbody>
                                <#list camions as camion>
                                    <tr>
                                        <td>
                                            <a href="${'/recyclApplication/camions/' + camion.noImattric}">${camion.noImattric}</a>
                                        </td>
                                        <td><#if camion.modele??>${camion.modele}<#else></#if></td>
                                        <td><#if camion.marque??>${camion.marque}<#else></#if></td>
                                        <td><#if (camion.dateAchat)??>${camion.dateAchat?date}</td></#if>
                                    </tr>
                                </#list>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
</html>