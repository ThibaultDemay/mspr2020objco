<#import "/spring.ftl" as spring/>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitionnal//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
    <#include "header.ftl">
</head>
<body>
<div class="wrapper">
    <#include "sidebar.ftl">
    <div id="content">
        <div class="container">
            <div class="card border-0 shadow my-5">
                <div class="card-body p-5">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <h2>La liste des demandes
                                <#if date1??>
                                    du ${date1} au ${date2}
                                <#else></#if>
                                <#if sansTournee??>
                                    à affecter à une tournée.
                                <#else></#if>
                            </h2>
                            <p><a href="${'/recyclApplication/index'}">Retourner à l'index</a></p>
                        </div>
                        <div class="panel-body">

                            <table class="table table-striped">
                                <thead>
                                <tr>
                                    <th>Numéro de Demande</th>
                                    <th>Date de la demande</th>
                                    <th>Date de l'enlèvement</th>
                                    <th>Web_O_N</th>
                                    <th>Siret de l'entreprise</th>
                                    <th>Numéro de tournée</th>
                                </tr>
                                </thead>
                                <tbody>
                                <#list demandes as demande>
                                    <tr>
                                        <td><a href="${'/recyclApplication/demandes/' + demande.no}">${demande.no}</a>
                                        </td>
                                        <td><#if demande.date??>${demande.date?datetime}<#else></#if></td>
                                        <td><#if demande.dateEnlevement??>${demande.dateEnlevement?datetime}<#else></#if></td>
                                        <td><#if demande.web_O_N??>${demande.web_O_N}<#else></#if></td>
                                        <td><#if demande.siret??>${demande.siret}<#else></#if></td>
                                        <td><#if demande.noTournee??>${demande.noTournee}<#else></#if></td>
                                    </tr>
                                </#list>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

</body>
</html>