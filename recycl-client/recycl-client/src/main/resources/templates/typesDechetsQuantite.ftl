<#import "/spring.ftl" as spring/>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitionnal//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
    <#include "header.ftl">
</head>
<body>
<div class="wrapper">
    <#include "sidebar.ftl">
    <div id="content">
        <div class="container">
            <div class="card border-0 shadow my-5">
                <div class="card-body p-5">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <h2>Quantités totales récoltées du ${date1} au ${date2}</h2>
                            <#if allCentres??> Echelle Nationale <#else> Au centre n° ${noCentre}</#if>
                            <p><a href="${'/recyclApplication/quantite-dechets-centre'}">Nouvelle recherche</a></p>
                            <p><a href="${'/recyclApplication/index'}">Retourner à l'index</a></p>
                        </div>
                        <div class="panel-body">

                            <table class="table table-striped">
                                <thead>
                                <tr>
                                    <th>Id</th>
                                    <th>Nom</th>
                                    <th>Niveau de danger</th>
                                    <th>Quantite totale ramassée (en tonnes)</th>

                                </tr>
                                </thead>
                                <tbody>
                                <#list typesDechetsQuantite as type>
                                    <tr>
                                        <td><#if type.idTypeDechet??>${type.idTypeDechet}<#else></#if></td>
                                        <td><#if type.nomTypeDechet??>${type.nomTypeDechet}<#else></#if></td>
                                        <td><#if type.niveauDangerTypeDechet??>${type.niveauDangerTypeDechet}<#else></#if></td>
                                        <td><#if type.quantiteTotale??>${type.quantiteTotale}<#else></#if></td>
                                    </tr>
                                </#list>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


</body>
</html>