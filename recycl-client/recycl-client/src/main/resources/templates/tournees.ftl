<#import "/spring.ftl" as spring/>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitionnal//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
    <#include "header.ftl">
</head>
<body>
<div class="wrapper">
    <#include "sidebar.ftl">
    <div id="content">
        <div class="container">
            <div class="card border-0 shadow my-5">
                <div class="card-body p-5">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <h2>La liste des tournées</h2>
                            <p><a href="${'/recyclApplication/index'}">Retourner à l'index</a></p>
                        </div>
                        <div class="panel-body">

                            <table class="table table-striped">
                                <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Date</th>
                                    <th>Immatriculation du camion</th>
                                    <th>No Employe</th>
                                </tr>
                                </thead>
                                <tbody>
                                <#list tournees as tournee>
                                    <tr>
                                        <td><a href="${'/recyclApplication/tournees/' + tournee.no}">${tournee.no}</td>
                                        <td>${tournee.dateTournee?date}</td>
                                        <td>
                                            <a href="${'/recyclApplication/camions/' + tournee.noImmatric}">${tournee.noImmatric}
                                        </td>
                                        <td>
                                            <a href="${'/recyclApplication/employes/' + tournee.noEmploye}">${tournee.noEmploye}
                                        </td>
                                    </tr>
                                </#list>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

</body>
</html>