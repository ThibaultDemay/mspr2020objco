<#import "/spring.ftl" as spring/>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitionnal//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
    <#include "header.ftl">
</head>
<body>
<div class="wrapper">
    <#include "sidebar.ftl">
    <div id="content">
        <div class="container">
            <div class="card border-0 shadow my-5">
                <div class="card-body p-5">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <h2>La liste des entreprises</h2>
                            <p><a href="${'/recyclApplication/index'}">Retourner à l'index</a></p>
                        </div>
                        <div class="panel-body">

                            <table class="table table-striped">
                                <thead>
                                <tr>
                                    <th>Siret</th>
                                    <th>Raison sociale</th>
                                    <th>N° rue</th>
                                    <th>Rue</th>
                                    <th>Code Postal</th>
                                    <th>Ville</th>
                                    <th>Téléphone</th>
                                    <th>Contact</th>
                                </tr>
                                </thead>
                                <tbody>
                                <#list entreprises as entreprise>
                                    <tr>
                                        <td>
                                            <a href="${'/recyclApplication/entreprises/' + entreprise.siret}">${entreprise.siret}</a>
                                        </td>
                                        <td><#if entreprise.raisonSociale??>${entreprise.raisonSociale}<#else></#if></td>
                                        <td><#if entreprise.noRue??>${entreprise.noRue}<#else></#if></td>
                                        <td><#if entreprise.rue??>${entreprise.rue}<#else></#if></td>
                                        <td><#if entreprise.cPostal??>${entreprise.cPostal}<#else></#if></td>
                                        <td><#if entreprise.ville??>${entreprise.ville}<#else></#if></td>
                                        <td><#if entreprise.noTel??>${entreprise.noTel}<#else></#if></td>
                                        <td><#if entreprise.contact??>${entreprise.contact}<#else></#if></td>
                                    </tr>
                                </#list>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

</body>
</html>