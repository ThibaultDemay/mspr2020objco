package com.mspr2020objco.recyclclient.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;
import java.util.List;


public class TypeDechet {

    @JsonProperty("notypedechet")
    private int no;
    @JsonProperty("nomtypedechet")
    private String nom;
    @JsonProperty("niv_danger")
    private int nivDanger;


    public TypeDechet(){
        super();
    }

    public TypeDechet(int no, String nom, int nivDanger) {
        this.no = no;
        this.nom = nom;
        this.nivDanger = nivDanger;
    }

    public int getNo() {
        return no;
    }

    public void setNo(int no) {
        this.no = no;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public int getNivDanger() {
        return nivDanger;
    }

    public void setNivDanger(int nivDanger) {
        this.nivDanger = nivDanger;
    }
}
