package com.mspr2020objco.recyclclient.service;

import com.mspr2020objco.recyclclient.model.Employe;
import com.mspr2020objco.recyclclient.model.EmployeTournee;
import com.mspr2020objco.recyclclient.wsclient.EmployeTourneeWebClientProvider;
import com.mspr2020objco.recyclclient.wsclient.EmployeWebClientProvider;;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class EmployeService {

    @Autowired
    private EmployeWebClientProvider employeWebClientProvider;

    @Autowired
    private EmployeTourneeWebClientProvider employeTourneeWebClientProvider;

    public List<Employe> getAll() {
        return employeWebClientProvider.findAll().collectList().block();
    }


    public Employe getById(String idValue) {
        return employeWebClientProvider.findById(idValue).block();
    }

    public List<EmployeTournee> getAllWithLessThanNTournees(int nbTournee) {
        return employeTourneeWebClientProvider.findAllWithLessThanNTournees(nbTournee).collectList().block();
    }
}
