package com.mspr2020objco.recyclclient.wsclient;

import com.mspr2020objco.recyclclient.model.DetailDemandeForDisplay;
import com.mspr2020objco.recyclclient.model.Employe;
import com.mspr2020objco.recyclclient.model.EmployeTournee;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Flux;

@Component
public class EmployeWebClientProvider extends WebClientProvider<Employe> {
    public EmployeWebClientProvider() {
        super(Employe.class, "noemploye");
    }


}


