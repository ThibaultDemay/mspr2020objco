package com.mspr2020objco.recyclclient.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class EmployeTournee {


    @JsonProperty("noemploye")
    private int no;
    @JsonProperty("nom")
    private String nom;
    @JsonProperty("prenom")
    private String prenom;
    @JsonProperty("nbtourneetotal")
    private int nbTourneeTotal;

    public EmployeTournee(int no, String nom, String prenom, int nbTourneeTotal) {
        this.no = no;
        this.nom = nom;
        this.prenom = prenom;
        this.nbTourneeTotal = nbTourneeTotal;
    }
    public EmployeTournee(){
        super();
    }

    public int getNo() {
        return no;
    }

    public void setNo(int no) {
        this.no = no;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public int getNbTourneeTotal() {
        return nbTourneeTotal;
    }

    public void setNbTourneeTotal(int nbTourneeTotal) {
        this.nbTourneeTotal = nbTourneeTotal;
    }
}




