package com.mspr2020objco.recyclclient.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;
import java.util.Date;
import java.util.List;


public class Demande {

    @JsonProperty("nodemande")
    private int no;
    @JsonProperty("datedemande")
    private Date date;
    @JsonProperty("dateenlevement")
    private Date dateEnlevement;
    @JsonProperty("web_o_n")
    private char web_O_N;
    @JsonProperty("siret")
    private String siret;
    @JsonProperty("notournee")
    private int noTournee;

    public Demande(int no, Date date, Date dateEnlevement, char web_O_N, String siret, int noTournee) {
        this.no = no;
        this.date = date;
        this.dateEnlevement = dateEnlevement;
        this.web_O_N = web_O_N;
        this.siret = siret;
        this.noTournee = noTournee;
    }

    public Demande(){
        super();
    }

    public int getNo() {
        return no;
    }

    public void setNo(int no) {
        this.no = no;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Date getDateEnlevement() {
        return dateEnlevement;
    }

    public void setDateEnlevement(Date dateEnlevement) {
        this.dateEnlevement = dateEnlevement;
    }

    public char getWeb_O_N() {
        return web_O_N;
    }

    public void setWeb_O_N(char web_O_N) {
        this.web_O_N = web_O_N;
    }

    public String getSiret() {
        return siret;
    }

    public void setSiret(String siret) {
        this.siret = siret;
    }

    public int getNoTournee() {
        return noTournee;
    }

    public void setNoTournee(int noTournee) {
        this.noTournee = noTournee;
    }
}
