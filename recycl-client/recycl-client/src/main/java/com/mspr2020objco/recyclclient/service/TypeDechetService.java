package com.mspr2020objco.recyclclient.service;

import com.mspr2020objco.recyclclient.model.TypeDechet;
import com.mspr2020objco.recyclclient.model.TypeDechetQuantite;
import com.mspr2020objco.recyclclient.wsclient.TypeDechetWebClientProvider;
import com.mspr2020objco.recyclclient.wsclient.TypeDechetQuantiteWebClientProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class TypeDechetService {

    @Autowired
    private TypeDechetWebClientProvider typeDechetWebClientProvider;
    @Autowired
    private TypeDechetQuantiteWebClientProvider typeDechetQuantiteWebClientProvider;

    public List<TypeDechet> getAll() {
        return typeDechetWebClientProvider.findAll().collectList().block();
    }

    public TypeDechet getById(String idValue) {
        return typeDechetWebClientProvider.findById(idValue).block();
    }

    public List<TypeDechetQuantite> getTypeDechetQuantiteBtwDates(String date1, String date2) {
        return typeDechetQuantiteWebClientProvider.getDechetQuantiteBtwDates(date1, date2).collectList().block();
    }

    public List<TypeDechetQuantite> getTypeDechetQuantiteByCentreTypeDechetBtwDates(int noCentre, String nomTypeDechet, String date1, String date2 ) {
        return typeDechetQuantiteWebClientProvider.getTypeDechetQuantiteByCentreTypeDechetBtwDates(noCentre, nomTypeDechet, date1, date2).collectList().block();
    }

    public List<TypeDechetQuantite> getTypeDechetQuantiteTypeDechetBtwDates(String nomTypeDechet, String date1, String date2 ) {
        return typeDechetQuantiteWebClientProvider.getTypeDechetQuantiteTypeDechetBtwDates(nomTypeDechet, date1, date2).collectList().block();
    }

    public List<TypeDechetQuantite> getTypeDechetQuantiteByCentreBtwDates(int noCentre, String date1, String date2 ) {
        return typeDechetQuantiteWebClientProvider.getTypeDechetQuantiteByCentreBtwDates(noCentre, date1, date2).collectList().block();
    }
}