package com.mspr2020objco.recyclclient.service;

import java.util.List;

import com.mspr2020objco.recyclclient.model.DetailDemandeForDisplay;
import com.mspr2020objco.recyclclient.wsclient.DetailDemandeForDisplayWebClientProvider;
import com.mspr2020objco.recyclclient.wsclient.DetailDemandeWebClientProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.mspr2020objco.recyclclient.model.Demande;
import com.mspr2020objco.recyclclient.wsclient.DemandeWebClientProvider;

@Component
public class DemandeService {

    @Autowired
    private DemandeWebClientProvider demandeWCProvider;
    @Autowired
    private DetailDemandeWebClientProvider detailDemandeWCProvider;
    @Autowired
    private DetailDemandeForDisplayWebClientProvider detailDemandeForDisplayWCProvider;

    public List<Demande> getAll() {
        return demandeWCProvider.findAll().collectList().block();
    }

    public Demande getById(String idValue) {
        return demandeWCProvider.findById(idValue).block();
    }

    public List<DetailDemandeForDisplay> getdetailForDisplayById(String numeroDemande) {
    	return detailDemandeForDisplayWCProvider.getDetailsDemandeForDisplay(numeroDemande).collectList().block();
    }

    public List<Demande> getAllBtw2Dates(String date1, String date2) {
        return demandeWCProvider.findAllBtw2Dates(date1, date2).collectList().block();
    }

    public List<Demande> getAllSansTournee() {
        return demandeWCProvider.findAllSansTournee().collectList().block();
    }

}