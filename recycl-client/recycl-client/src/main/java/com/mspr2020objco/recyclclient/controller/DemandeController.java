package com.mspr2020objco.recyclclient.controller;

import com.mspr2020objco.recyclclient.model.Demande;
import com.mspr2020objco.recyclclient.model.DetailDemande;
import com.mspr2020objco.recyclclient.model.DetailDemandeForDisplay;
import com.mspr2020objco.recyclclient.model.TypeDechetQuantite;
import com.mspr2020objco.recyclclient.service.DemandeService;
import com.mspr2020objco.recyclclient.service.DetailDemandeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
public class DemandeController {

    @Autowired
    private DemandeService demandeService;

    @Autowired
    private DetailDemandeService detailDemandeService;

    @GetMapping("/demandes")
    public ModelAndView showDemandes(Model model) {
        List<Demande> demandesList = this.demandeService.getAll();
        Map<String, Object> params = new HashMap<>();
        params.put("demandes", demandesList);
        return new ModelAndView("demandes", params);
    }

    @GetMapping("/demandes/{id}")
    public ModelAndView showDemandes(Model model, @PathVariable String id) {
        Demande demande = demandeService.getById(id);
        List<DetailDemande> detailDemandeList = detailDemandeService.getAllByDemande(id);
        List<DetailDemandeForDisplay> detailDemandeForDisplayList = demandeService.getdetailForDisplayById(id);
        Map<String, Object> params = new HashMap<>();
        params.put("demande", demande);
        //params.put("detailsdemande", detailDemandeList);
        params.put("raisonSociale", detailDemandeForDisplayList.get(0).getRaisonSociale());
        params.put("detailsdemandeForDisplay", detailDemandeForDisplayList);
        return new ModelAndView("demande", params);
    }

    @GetMapping("/demandesSansTournee")
    public ModelAndView showDemandesSansTournee(Model model) {
        List<Demande> demandeList = demandeService.getAllSansTournee();
        Map<String, Object> params = new HashMap<>();
        params.put("demandes", demandeList);
        params.put("sansTournee", true);

        return new ModelAndView("demandes", params);
    }

    @GetMapping("/demandesdatesSpeciales")
    public ModelAndView showDemandesBtw2Dates(Model model) {
        String date1 = "2018-08-01";
        String date2 = "2018-08-31";
        List<Demande> demandeList = demandeService.getAllBtw2Dates(date1, date2);
        Map<String, Object> params = new HashMap<>();
        params.put("demandes", demandeList);
        params.put("date1", date1);
        params.put("date2", date2);
        return new ModelAndView("demandes", params);
    }


    @GetMapping("/demandesDates")
    public ModelAndView showDemandesBtw2DatesForm(Model model) {
        return new ModelAndView("demandesDatesForm");
    }


    @PostMapping("/demandesDates")
    public ModelAndView submitDemandesBtw2DatesForm(Model model, @RequestParam(name = "date1") String date1,
                                                    @RequestParam(name = "date2") String date2) {
        List<Demande> demandeList = demandeService.getAllBtw2Dates(date1, date2);
        Map<String, Object> params = new HashMap<>();
        params.put("demandes", demandeList);
        params.put("date1", date1);
        params.put("date2", date2);
        return new ModelAndView("demandes", params);
    }
}
