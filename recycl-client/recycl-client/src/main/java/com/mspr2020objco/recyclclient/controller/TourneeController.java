package com.mspr2020objco.recyclclient.controller;

import com.mspr2020objco.recyclclient.model.Tournee;
import com.mspr2020objco.recyclclient.model.Tournee;
import com.mspr2020objco.recyclclient.service.TourneeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.servlet.ModelAndView;
import reactor.core.publisher.Flux;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
public class TourneeController {

    @Autowired
    private TourneeService tourneeService;

    @GetMapping("/tournees")
    public ModelAndView showTournees(Model model){
        List<Tournee> tourneesList = this.tourneeService.getAll();
        Map<String, Object> params = new HashMap<>();
        params.put("tournees", tourneesList);
        return new ModelAndView("tournees", params);
    }
    @GetMapping("/tournees/{id}")
    public ModelAndView showTournees(Model model, @PathVariable String id){
        Tournee tournee = tourneeService.getById(id);
        Map<String, Object> params = new HashMap<>();
        params.put("tournee", tournee);
        return new ModelAndView("tournee", params);
    }

}