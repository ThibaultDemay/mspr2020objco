package com.mspr2020objco.recyclclient.service;

import com.mspr2020objco.recyclclient.model.Demande;
import com.mspr2020objco.recyclclient.model.DetailDemande;
import com.mspr2020objco.recyclclient.wsclient.DemandeWebClientProvider;
import com.mspr2020objco.recyclclient.wsclient.DetailDemandeWebClientProvider;
import com.mspr2020objco.recyclclient.wsclient.WebClientProvider;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.List;

@Component
public class DetailDemandeService {


    @Autowired
    private DetailDemandeWebClientProvider detailDemandeWebClientProvider;

    private String idAttribute = "nodemande";

    public List<DetailDemande> getAll() {
        return detailDemandeWebClientProvider.findAll().collectList().block();
    }

    public List<DetailDemande> getAllByDemande(String noDemande) {
        return detailDemandeWebClientProvider.findAllByDemande(noDemande).collectList().block();
    }

}