package com.mspr2020objco.recyclclient.wsclient;

import org.springframework.stereotype.Component;

import com.mspr2020objco.recyclclient.model.Demande;
import com.mspr2020objco.recyclclient.model.DetailDemandeForDisplay;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Component
public class DetailDemandeForDisplayWebClientProvider extends WebClientProvider<DetailDemandeForDisplay> {

    public DetailDemandeForDisplayWebClientProvider() {
        super(DetailDemandeForDisplay.class, "nodemande");
    }


    public Flux<DetailDemandeForDisplay> getDetailsDemandeForDisplay(String numeroDemande) {
        return getSpecificRequestWebclient("2").get()
                .uri(uriBuilder -> uriBuilder
                        .queryParam("nodemande", numeroDemande)
                        .build())
                .retrieve()
                .bodyToFlux(clazz);
    }

}