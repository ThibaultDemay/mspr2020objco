package com.mspr2020objco.recyclclient.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class TypeDechetQuantite {

    @JsonProperty("quantitedeposeetotal")
    private int quantiteTotale;
    @JsonProperty("notypedechet")
    private String idTypeDechet;
    @JsonProperty("nomtypedechet")
    private String nomTypeDechet;
    @JsonProperty("niv_danger")
    private String niveauDangerTypeDechet;

    public int getQuantiteTotale() {
        return quantiteTotale;
    }

    public void setQuantiteTotale(int quantiteTotale) {
        this.quantiteTotale = quantiteTotale;
    }

    public String getIdTypeDechet() {
        return idTypeDechet;
    }

    public void setIdTypeDechet(String idTypeDechet) {
        this.idTypeDechet = idTypeDechet;
    }

    public String getNomTypeDechet() {
        return nomTypeDechet;
    }

    public void setNomTypeDechet(String nomTypeDechet) {
        this.nomTypeDechet = nomTypeDechet;
    }

    public String getNiveauDangerTypeDechet() {
        return niveauDangerTypeDechet;
    }

    public void setNiveauDangerTypeDechet(String niveauDangerTypeDechet) {
        this.niveauDangerTypeDechet = niveauDangerTypeDechet;
    }
}
