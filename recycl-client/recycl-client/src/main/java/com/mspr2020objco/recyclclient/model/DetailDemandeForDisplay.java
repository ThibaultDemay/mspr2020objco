package com.mspr2020objco.recyclclient.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonSetter;

public class DetailDemandeForDisplay {
	
	@JsonProperty("raisonsociale")
	private String raisonSociale;
	@JsonProperty("notournee")
	private String noTournee;
	@JsonProperty("nomtypedechet")
	private String nomTypeDechet;
	@JsonProperty("quantiteenlevee")
	private String quantiteEnlevee;
	@JsonProperty("remarque")
	private String remarque;

	private Tournee tournee;



	public String getRaisonSociale() {
		return raisonSociale;
	}
	
	public void setRaisonSociale(String raisonSociale) {
		this.raisonSociale = raisonSociale;
	}
	
	public Tournee getTournee() {
		return this.tournee;
	}
	
	public void setTournee(Tournee tournee) {
		this.tournee = tournee;
	}
	
//	@JsonSetter(value = "notournee")
//	public void setTournee(Integer numeroTournee) {
//		this.tournee = new Tournee();
//		tournee.setNo(numeroTournee);
//	}

	public String getNoTournee() {
		return noTournee;
	}

	public void setNoTournee(String noTournee) {
		this.noTournee = noTournee;
	}

	public String getNomTypeDechet() {
		return nomTypeDechet;
	}

	public void setNomTypeDechet(String nomTypeDechet) {
		this.nomTypeDechet = nomTypeDechet;
	}

	public String getQuantiteEnlevee() {
		return quantiteEnlevee;
	}

	public void setQuantiteEnlevee(String quantiteEnlevee) {
		this.quantiteEnlevee = quantiteEnlevee;
	}

	public String getRemarque() {
		return remarque;
	}

	public void setRemarque(String remarque) {
		this.remarque = remarque;
	}
}
