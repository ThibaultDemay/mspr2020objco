package com.mspr2020objco.recyclclient.service;

import com.mspr2020objco.recyclclient.model.Camion;
import com.mspr2020objco.recyclclient.wsclient.CamionWebClientProvider;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class CamionService {

    @Autowired
    private CamionWebClientProvider camionWebClientProvider;

    public List<Camion> getAll() {
        return camionWebClientProvider.findAll().collectList().block();
    }

    public Camion getById(String idValue) {
        return camionWebClientProvider.findById(idValue).block();
    }
}