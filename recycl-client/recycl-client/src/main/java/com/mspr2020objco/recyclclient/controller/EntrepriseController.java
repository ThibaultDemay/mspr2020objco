package com.mspr2020objco.recyclclient.controller;

import com.mspr2020objco.recyclclient.model.Entreprise;
import com.mspr2020objco.recyclclient.model.EntrepriseNombreDemande;
import com.mspr2020objco.recyclclient.service.EntrepriseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
public class EntrepriseController {

    @Autowired
    private EntrepriseService entrepriseService;

    @GetMapping("/entreprises")
    public ModelAndView showEntreprises(Model model) {
        List<Entreprise> entreprisesList = this.entrepriseService.getAll();
        Map<String, Object> params = new HashMap<>();
        params.put("entreprises", entreprisesList);
        return new ModelAndView("entreprises", params);
    }

    @GetMapping("/entreprises/{id}")
    public ModelAndView showEntreprises(Model model, @PathVariable String id) {
        Entreprise entreprise = entrepriseService.getById(id);
        Map<String, Object> params = new HashMap<>();
        params.put("entreprise", entreprise);
        return new ModelAndView("entreprise", params);
    }


    @GetMapping("/entreprisesNombreDemandes")
    public ModelAndView showEmployesTourneesForm(Model model) {
        List<Entreprise> entreprisesList = this.entrepriseService.getAll();
        Map<String, Object> params = new HashMap<>();
        params.put("entreprises", entreprisesList);
        return new ModelAndView("entrepriseNombreDemandeForm", params);
    }

    @PostMapping("/entreprisesNombreDemandes")
    public ModelAndView submitEmployesTourneesForm(Model model,
                                                   @RequestParam(name = "raisonSocialeEntrepriseSelect", required = true) String raisonSociale) {
        List<EntrepriseNombreDemande> entreprisesList = this.entrepriseService.getAllWithMoreDemandesThan(raisonSociale);
        Map<String, Object> params = new HashMap<>();
        params.put("entreprises", entreprisesList);
        params.put("raisonSociale", raisonSociale);
        return new ModelAndView("entreprisesNombreTournees", params);
    }

}
