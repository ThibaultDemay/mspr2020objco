package com.mspr2020objco.recyclclient.service;

import com.mspr2020objco.recyclclient.model.Tournee;
import com.mspr2020objco.recyclclient.model.Tournee;
import com.mspr2020objco.recyclclient.wsclient.TourneeWebClientProvider;
import com.mspr2020objco.recyclclient.wsclient.WebClientProvider;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.List;

@Component
public class TourneeService {

    @Autowired
    private TourneeWebClientProvider tourneeWebClientProvider;

    public List<Tournee> getAll() {
        return tourneeWebClientProvider.findAll().collectList().block();
    }

    public Tournee getById(String idValue) {
        return tourneeWebClientProvider.findById(idValue).block();
    }
}

