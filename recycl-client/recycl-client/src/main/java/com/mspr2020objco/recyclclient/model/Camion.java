package com.mspr2020objco.recyclclient.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.Date;


public class Camion {

    @JsonProperty("noimmatric")
    private String noImattric;
    @JsonProperty("dateachat")
    private Date dateAchat;
    @JsonProperty("modele")
    private String modele;
    @JsonProperty("marque")
    private String marque;


    public Camion(String noImattric, Date dateAchat, String modele, String marque) {
        this.noImattric = noImattric;
        this.dateAchat = dateAchat;
        this.modele = modele;
        this.marque = marque;
    }

    public Camion(){
        super();
    }


    public String getNoImattric() {
        return noImattric;
    }

    public void setNoImattric(String noImattric) {
        this.noImattric = noImattric;
    }

    public Date getDateAchat() {
        return dateAchat;
    }

    public void setDateAchat(Date dateAchat) {
        this.dateAchat = dateAchat;
    }

    public String getModele() {
        return modele;
    }

    public void setModele(String modele) {
        this.modele = modele;
    }

    public String getMarque() {
        return marque;
    }

    public void setMarque(String marque) {
        this.marque = marque;
    }

}
