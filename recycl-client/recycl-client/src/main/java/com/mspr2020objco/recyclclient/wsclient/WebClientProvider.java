package com.mspr2020objco.recyclclient.wsclient;

import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.web.reactive.function.client.WebClient;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public abstract class WebClientProvider<T> {

    protected Class<T> clazz;
	protected String idFieldName;
    protected WebClient webClient;

    private String urlAPI = "http://msprobjco.com/Objco/";
    private String urlAPIForSpecificRequest = "http://msprobjco.com/Objco/requetes/";

    public WebClientProvider(Class<T> clazz, String idFieldName) {
    	this.clazz = clazz;
    	this.idFieldName = idFieldName;
    	
        this.webClient = WebClient.builder()
                .baseUrl(urlAPI + clazz.getSimpleName().toLowerCase() + '/')
                .defaultHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .defaultHeader(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_VALUE)
                .codecs(configurer -> configurer.defaultCodecs().enableLoggingRequestDetails(true))
                .build();
    }

    public WebClient getSpecificRequestWebclient(String requestId){
        return  WebClient.builder()
                .baseUrl(urlAPIForSpecificRequest + requestId + ".php")
                .defaultHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .defaultHeader(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_VALUE)
                .codecs(configurer -> configurer.defaultCodecs().enableLoggingRequestDetails(true))
                .build();
    }

    public Flux<T> findAll() {
        return webClient.get()
                .retrieve()
                .bodyToFlux(clazz);
    }

    public Mono<T> findById(String idValue) {
        return webClient.get().uri(uriBuilder -> uriBuilder.queryParam(idFieldName, idValue).build())
                .retrieve()
                .bodyToMono(clazz);
    }

}
