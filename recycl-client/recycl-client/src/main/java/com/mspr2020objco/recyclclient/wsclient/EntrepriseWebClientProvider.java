package com.mspr2020objco.recyclclient.wsclient;

import com.mspr2020objco.recyclclient.model.Entreprise;
import org.springframework.stereotype.Component;

@Component
public class EntrepriseWebClientProvider extends WebClientProvider<Entreprise> {
    public EntrepriseWebClientProvider() {
        super(Entreprise.class, "siret");
    }
}