package com.mspr2020objco.recyclclient.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class EntrepriseNombreDemande {

    @JsonProperty("raisonsociale")
    private String raisonSociale;
    @JsonProperty("nombre")
    private int nombreDemandes;

    public EntrepriseNombreDemande(String raisonSociale, int nombreDemandes) {
        this.nombreDemandes = nombreDemandes;
        this.raisonSociale = raisonSociale;

    }

    public EntrepriseNombreDemande() {
        super();
    }

    public String getRaisonSociale() {
        return raisonSociale;
    }

    public void setRaisonSociale(String raisonSociale) {
        this.raisonSociale = raisonSociale;
    }

    public int getNombreDemandes() {
        return nombreDemandes;
    }

    public void setNombreDemandes(int nombreDemandes) {
        this.nombreDemandes = nombreDemandes;
    }
}




