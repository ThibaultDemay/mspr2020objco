package com.mspr2020objco.recyclclient.controller;

import com.mspr2020objco.recyclclient.model.Demande;
import com.mspr2020objco.recyclclient.model.DetailDemande;
import com.mspr2020objco.recyclclient.service.DemandeService;
import com.mspr2020objco.recyclclient.service.DetailDemandeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.servlet.ModelAndView;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DetailDemandeController {


    @Autowired
    private DetailDemandeService detailDemandeService;

    @GetMapping("/detailsdemandes")
    public ModelAndView showDemandes(Model model){
        List<DetailDemande> demandesList = this.detailDemandeService.getAll();
        Map<String, Object> params = new HashMap<>();
        params.put("demandes", demandesList);
        return new ModelAndView("demandes", params);
    }

}
