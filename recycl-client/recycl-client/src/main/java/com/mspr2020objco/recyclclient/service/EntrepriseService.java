package com.mspr2020objco.recyclclient.service;


import com.mspr2020objco.recyclclient.model.Employe;
import com.mspr2020objco.recyclclient.model.Entreprise;
import com.mspr2020objco.recyclclient.model.EntrepriseNombreDemande;
import com.mspr2020objco.recyclclient.wsclient.EntrepriseNombreDemandeWebClientProvider;
import com.mspr2020objco.recyclclient.wsclient.EntrepriseWebClientProvider;
import com.mspr2020objco.recyclclient.wsclient.WebClientProvider;
import com.mspr2020objco.recyclclient.model.Entreprise;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.List;

@Component
public class EntrepriseService {

    @Autowired
    private EntrepriseWebClientProvider entrepriseWebClientProvider;
    @Autowired
    private EntrepriseNombreDemandeWebClientProvider entrepriseNombreDemandeWebClientProvider;

    public List<Entreprise> getAll() {
        return entrepriseWebClientProvider.findAll().collectList().block();
    }

    public Entreprise getById(String idValue) {
        return entrepriseWebClientProvider.findById(idValue).block();
    }

    public List<EntrepriseNombreDemande> getAllWithMoreDemandesThan(String raisonSocial) {
        return entrepriseNombreDemandeWebClientProvider.findAllWithMoreDemandesThan(raisonSocial).collectList().block();
    }
}

