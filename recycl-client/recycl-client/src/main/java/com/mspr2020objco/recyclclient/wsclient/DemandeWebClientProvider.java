package com.mspr2020objco.recyclclient.wsclient;

import org.springframework.stereotype.Component;

import com.mspr2020objco.recyclclient.model.Demande;
import com.mspr2020objco.recyclclient.model.DetailDemandeForDisplay;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Component
public class DemandeWebClientProvider extends WebClientProvider<Demande> {

    public DemandeWebClientProvider() {
		super(Demande.class, "nodemande");
	}


    // Permet de répondre à la requête 1 :
    // http://msprobjco.com/Objco/requetes/1.php?date1=2018-08-01&date2=2018-08-31
    public Flux<Demande> findAllBtw2Dates(String date1, String date2) {
        // TODO : Check du format des dates ?
        return getSpecificRequestWebclient("1").get()
                .uri(uriBuilder -> uriBuilder
                .queryParam("date1", date1)
                .queryParam("date2", date2)
                .build())
                .retrieve()
                .bodyToFlux(clazz);
    }

    // Permet de répondre à la requête 6 :
    // http://msprobjco.com/Objco/requetes/6.php
    public Flux<Demande> findAllSansTournee() {
        return getSpecificRequestWebclient("6").get()
                .uri(uriBuilder -> uriBuilder
                        .build())
                .retrieve()
                .bodyToFlux(clazz);
    }

}