package com.mspr2020objco.recyclclient.controller;

import com.mspr2020objco.recyclclient.model.CentreTraitement;
import com.mspr2020objco.recyclclient.service.CentreTraitementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.servlet.ModelAndView;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
@Controller
public class CentreTraitementController {

    @Autowired
    private CentreTraitementService centreTraitementService;

    @GetMapping("/centres-traitement")
    public ModelAndView showCentreTraitements(Model model){
        List<CentreTraitement> centreTraitementList = this.centreTraitementService.getAll();
        Map<String, Object> params = new HashMap<>();
        params.put("centres", centreTraitementList);
        return new ModelAndView("centres", params);
    }

    @GetMapping("/centres-traitement/{id}")
    public ModelAndView showCentreTraitements(Model model, @PathVariable String id){
        CentreTraitement centreTraitement = centreTraitementService.getById(id);
        Map<String, Object> params = new HashMap<>();
        params.put("centre", centreTraitement);
        return new ModelAndView("centre", params);
    }
}
