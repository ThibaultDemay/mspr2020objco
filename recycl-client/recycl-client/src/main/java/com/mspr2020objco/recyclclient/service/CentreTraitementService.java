package com.mspr2020objco.recyclclient.service;


import com.mspr2020objco.recyclclient.model.CentreTraitement;
import com.mspr2020objco.recyclclient.wsclient.CentreTraitementWebClientProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class CentreTraitementService {

    @Autowired
    private CentreTraitementWebClientProvider centreTraitementWebClientProvider;

    public List<CentreTraitement> getAll() {
        return centreTraitementWebClientProvider.findAll().collectList().block();
    }

    public CentreTraitement getById(String idValue) {
        return centreTraitementWebClientProvider.findById(idValue).block();
    }
}