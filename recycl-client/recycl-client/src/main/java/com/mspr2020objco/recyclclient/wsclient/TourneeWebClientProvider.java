package com.mspr2020objco.recyclclient.wsclient;

import com.mspr2020objco.recyclclient.model.Tournee;
import org.springframework.stereotype.Component;

@Component
public class TourneeWebClientProvider extends WebClientProvider<Tournee> {
    public TourneeWebClientProvider() {
        super(Tournee.class, "notournee");
    }
}