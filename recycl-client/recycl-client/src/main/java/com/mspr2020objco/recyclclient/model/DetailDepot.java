package com.mspr2020objco.recyclclient.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;

public class DetailDepot {

    @JsonProperty("quantitedeposee")
    private int quantiteDeposee;
    @JsonProperty("notournee")
    private Tournee tournee;
    @JsonProperty("notypedechet")
    private int  notypedechet;
    @JsonProperty("nocentre")
    private int nocentre;


    public DetailDepot(int quantiteDeposee, Tournee tournee, int notypedechet, int nocentre) {
        this.quantiteDeposee = quantiteDeposee;
        this.tournee = tournee;
        this.nocentre = nocentre;
        this.notypedechet = notypedechet;
    }

    public DetailDepot(){
        super();
    }

    public int getQuantiteDeposee() {
        return quantiteDeposee;
    }

    public void setQuantiteDeposee(int quantiteDeposee) {
        this.quantiteDeposee = quantiteDeposee;
    }

    public Tournee getTournee() {
        return tournee;
    }

    public void setTournee(Tournee tournee) {
        this.tournee = tournee;
    }

    public int getNotypedechet() {
        return notypedechet;
    }

    public void setNotypedechet(int notypedechet) {
        this.notypedechet = notypedechet;
    }

    public int getNocentre() {
        return nocentre;
    }

    public void setNocentre(int nocentre) {
        this.nocentre = nocentre;
    }
}
