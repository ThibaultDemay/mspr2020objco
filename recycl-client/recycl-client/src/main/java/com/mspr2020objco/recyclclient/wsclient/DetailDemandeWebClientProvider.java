package com.mspr2020objco.recyclclient.wsclient;

import com.mspr2020objco.recyclclient.model.Camion;
import com.mspr2020objco.recyclclient.model.Demande;
import com.mspr2020objco.recyclclient.model.DetailDemande;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Component
public class DetailDemandeWebClientProvider extends WebClientProvider<DetailDemande>{
    public DetailDemandeWebClientProvider() {
        super(DetailDemande.class, "nodemande");
    }

    // Ici il n'y a pas d'ID, cette méthode n'aurait pas de sens
    @Override
    public Mono<DetailDemande> findById(String idValue) {
        throw new RuntimeException("Attention : Cette classe ne dispose pas d'id unique");
    }


    public Flux<DetailDemande> findAllByDemande(String noDemande) {
        return webClient.get().uri(uriBuilder -> uriBuilder.queryParam(idFieldName, noDemande).build())
                .retrieve()
                .bodyToFlux(clazz);
    }


}
