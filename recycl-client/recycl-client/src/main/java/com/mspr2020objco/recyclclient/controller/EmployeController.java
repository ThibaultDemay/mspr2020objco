package com.mspr2020objco.recyclclient.controller;

import com.mspr2020objco.recyclclient.model.CentreTraitement;
import com.mspr2020objco.recyclclient.model.Employe;
import com.mspr2020objco.recyclclient.model.EmployeTournee;
import com.mspr2020objco.recyclclient.model.TypeDechet;
import com.mspr2020objco.recyclclient.service.EmployeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
public class EmployeController {

    @Autowired
    private EmployeService employeService;

    @GetMapping("/employes")
    public ModelAndView show(Model model){
        List<Employe> employesList = this.employeService.getAll();
        Map<String, Object> params = new HashMap<>();
        params.put("employes", employesList);
        return new ModelAndView("employes", params);
    }

    @GetMapping("/employes/{id}")
    public ModelAndView showEmployes(Model model, @PathVariable String id){
        Employe employe = employeService.getById(id);
        Map<String, Object> params = new HashMap<>();
        params.put("employe", employe);
        return new ModelAndView("employe", params);
    }


    @GetMapping("/employes-inf-tournee")
    public ModelAndView showEmployesTourneesForm(Model model) {
        return new ModelAndView("employesNbTourneeForm");
    }

    @PostMapping("/employes-inf-tournee")
    public ModelAndView submitEmployesTourneesForm(Model model, @RequestParam(name="nbTournees") int nbtournee) {
        List<EmployeTournee> employesList = this.employeService.getAllWithLessThanNTournees(nbtournee);
        Map<String, Object> params = new HashMap<>();
        params.put("employes", employesList);
        params.put("nbTournees", nbtournee);
        return new ModelAndView("employesTournee", params);
    }


}
