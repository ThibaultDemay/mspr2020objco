package com.mspr2020objco.recyclclient.wsclient;

import com.mspr2020objco.recyclclient.model.TypeDechet;
import org.springframework.stereotype.Component;

@Component
public class TypeDechetWebClientProvider extends WebClientProvider<TypeDechet> {

    public TypeDechetWebClientProvider() {
        super(TypeDechet.class, "notypedechet");
    }

}
