package com.mspr2020objco.recyclclient.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;
import java.util.List;

public class CentreTraitement {

    @JsonProperty("nocentre")
    private int no;
    @JsonProperty("nomcentre")
    private String nom;
    @JsonProperty("noruecentre")
    private int noRue;
    @JsonProperty("ruecentre")
    private String rue;
    @JsonProperty("cpostalcentre")
    private int cPostal;
    @JsonProperty("villecentre")
    private String ville;

    public CentreTraitement(int no, String nom, int noRue, String rue, int cPostal, String ville) {
        this.no = no;
        this.nom = nom;
        this.noRue = noRue;
        this.rue = rue;
        this.cPostal = cPostal;
        this.ville = ville;
    }

    public CentreTraitement(){
        super();
    }

    public int getNo() {
        return no;
    }

    public void setNo(int no) {
        this.no = no;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public int getNoRue() {
        return noRue;
    }

    public void setNoRue(int noRue) {
        this.noRue = noRue;
    }

    public String getRue() {
        return rue;
    }

    public void setRue(String rue) {
        this.rue = rue;
    }

    public int getcPostal() {
        return cPostal;
    }

    public void setcPostal(int cPostal) {
        this.cPostal = cPostal;
    }

    public String getVille() {
        return ville;
    }

    public void setVille(String ville) {
        this.ville = ville;
    }
}
