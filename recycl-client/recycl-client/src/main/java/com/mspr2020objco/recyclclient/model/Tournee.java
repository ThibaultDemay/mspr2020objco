package com.mspr2020objco.recyclclient.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Date;

public class Tournee {

    @JsonProperty("notournee")
    private int no;
    @JsonProperty("datetournee")
    private Date dateTournee;
    @JsonProperty("noemploye")
    private int noEmploye;
    @JsonProperty("noimmatric")
    private String noImmatric ;

    public Tournee(int no, Date dateTournee, int noEmploye, String noimmatric) {
        this.no = no;
        this.dateTournee = dateTournee;
        this.noEmploye = noEmploye;
        this.noImmatric = noimmatric;
    }

    public Tournee() {
        super();
    }

    public int getNo() {
        return no;
    }

    public void setNo(int no) {
        this.no = no;
    }

    public Date getDateTournee() {
        return dateTournee;
    }

    public void setDateTournee(Date dateTournee) {
        this.dateTournee = dateTournee;
    }

    public int getNoEmploye() {
        return noEmploye;
    }

    public void setNoEmploye(int noEmploye) {
        this.noEmploye = noEmploye;
    }

    public String getNoImmatric() {
        return noImmatric;
    }

    public void setNoImmatric(String noImmatric) {
        this.noImmatric = noImmatric;
    }
}
