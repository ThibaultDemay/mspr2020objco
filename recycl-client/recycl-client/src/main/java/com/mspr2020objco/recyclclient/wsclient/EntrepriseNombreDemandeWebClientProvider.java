package com.mspr2020objco.recyclclient.wsclient;

import com.mspr2020objco.recyclclient.model.EmployeTournee;
import com.mspr2020objco.recyclclient.model.EntrepriseNombreDemande;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Flux;
@Component
public class EntrepriseNombreDemandeWebClientProvider extends WebClientProvider<EntrepriseNombreDemande> {


        public EntrepriseNombreDemandeWebClientProvider() {
            super(EntrepriseNombreDemande.class, "siret");
        }

        public Flux<EntrepriseNombreDemande> findAllWithMoreDemandesThan(String raisonSociale) {
            return getSpecificRequestWebclient("5").get()
                    .uri(uriBuilder -> uriBuilder
                            .queryParam("raison", raisonSociale)
                            .build())
                    .retrieve()
                    .bodyToFlux(clazz);
        }

}
