package com.mspr2020objco.recyclclient.controller;

import com.mspr2020objco.recyclclient.model.Camion;
import com.mspr2020objco.recyclclient.service.CamionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.servlet.ModelAndView;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
public class CamionController {

    @Autowired
    private CamionService camionService;

    @GetMapping("/camions")
    public ModelAndView showCamions(Model model){
        List<Camion> camionsList = this.camionService.getAll();
        Map<String, Object> params = new HashMap<>();
        params.put("camions", camionsList);
        return new ModelAndView("camions", params);
    }

    @GetMapping("/camions/{id}")
    public ModelAndView showCamions(Model model, @PathVariable String id){
        Camion camion = camionService.getById(id);
        Map<String, Object> params = new HashMap<>();
        params.put("camion", camion);
        return new ModelAndView("camion", params);
    }


}
