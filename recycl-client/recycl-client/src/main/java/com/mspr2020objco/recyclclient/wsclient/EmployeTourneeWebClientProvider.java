package com.mspr2020objco.recyclclient.wsclient;

import com.mspr2020objco.recyclclient.model.EmployeTournee;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Flux;

@Component
public class EmployeTourneeWebClientProvider extends WebClientProvider<EmployeTournee> {
    public EmployeTourneeWebClientProvider() {
        super(EmployeTournee.class, "noemploye");
    }

    public Flux<EmployeTournee> findAllWithLessThanNTournees(int nbTournee) {
        return getSpecificRequestWebclient("4").get()
                .uri(uriBuilder -> uriBuilder
                        .queryParam("nb", nbTournee)
                        .build())
                .retrieve()
                .bodyToFlux(clazz);
    }
}


