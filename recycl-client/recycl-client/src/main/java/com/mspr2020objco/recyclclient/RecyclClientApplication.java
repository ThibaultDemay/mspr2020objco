package com.mspr2020objco.recyclclient;


import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class RecyclClientApplication implements CommandLineRunner {


    public static void main(String[] args) {
        SpringApplication.run(RecyclClientApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
    }
}
