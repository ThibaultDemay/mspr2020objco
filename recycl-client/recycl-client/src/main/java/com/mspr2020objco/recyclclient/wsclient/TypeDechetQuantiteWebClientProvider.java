package com.mspr2020objco.recyclclient.wsclient;

import com.mspr2020objco.recyclclient.model.TypeDechetQuantite;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Flux;

@Component
public class TypeDechetQuantiteWebClientProvider extends WebClientProvider<TypeDechetQuantite> {

    public TypeDechetQuantiteWebClientProvider() {
        super(TypeDechetQuantite.class, "notypedechet");
    }

    public Flux<TypeDechetQuantite> getDechetQuantiteBtwDates(String date1, String date2) {
        return getSpecificRequestWebclient("3").get()
                .uri(uriBuilder -> uriBuilder
                        .queryParam("date1", date1)
                        .queryParam("date2", date2)
                        .build())
                .retrieve()
                .bodyToFlux(clazz);
    }

    public Flux<TypeDechetQuantite> getTypeDechetQuantiteByCentreTypeDechetBtwDates(int noCentre, String nomTypeDechet, String date1, String date2) {
        return getSpecificRequestWebclient("7").get()
                .uri(uriBuilder -> uriBuilder
                        .queryParam("date1", date1)
                        .queryParam("date2", date2)
                        .queryParam("nocentre", noCentre)
                        .queryParam("nomtypedechet", nomTypeDechet)
                        .build())
                .retrieve()
                .bodyToFlux(clazz);
    }

    public Flux<TypeDechetQuantite> getTypeDechetQuantiteTypeDechetBtwDates(String nomTypeDechet, String date1, String date2) {
        return getSpecificRequestWebclient("8").get()
                .uri(uriBuilder -> uriBuilder
                        .queryParam("date1", date1)
                        .queryParam("date2", date2)
                        .queryParam("nomtypedechet", nomTypeDechet)
                        .build())
                .retrieve()
                .bodyToFlux(clazz);
    }

    public Flux<TypeDechetQuantite> getTypeDechetQuantiteByCentreBtwDates(int noCentre, String date1, String date2) {
        return getSpecificRequestWebclient("9").get()
                .uri(uriBuilder -> uriBuilder
                        .queryParam("date1", date1)
                        .queryParam("date2", date2)
                        .queryParam("nocentre", noCentre)
                        .build())
                .retrieve()
                .bodyToFlux(clazz);
    }


}
