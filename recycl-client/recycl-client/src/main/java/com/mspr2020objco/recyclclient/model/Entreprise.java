package com.mspr2020objco.recyclclient.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;
import java.util.List;


public class Entreprise {

    @JsonProperty("siret")
    private String siret;
    @JsonProperty("raisonsociale")
    private String raisonSociale;
    @JsonProperty("norueentr")
    private int noRue;
    @JsonProperty("rueentr")
    private String rue;
    @JsonProperty("cpostalentr")
    private int cPostal;
    @JsonProperty("villeentr")
    private String ville;
    @JsonProperty("notel")
    private int noTel;
    @JsonProperty("contact")
    private String contact;

    public Entreprise(String siret, String raisonSociale, int noRue, String rue, int cPostal, String ville, int noTel, String contact) {
        this.siret = siret;
        this.raisonSociale = raisonSociale;
        this.noRue = noRue;
        this.rue = rue;
        this.cPostal = cPostal;
        this.ville = ville;
        this.noTel = noTel;
        this.contact = contact;
    }

    public Entreprise() {
        super();
    }

    public String getSiret() {
        return siret;
    }

    public void setSiret(String siret) {
        this.siret = siret;
    }

    public String getRaisonSociale() {
        return raisonSociale;
    }

    public void setRaisonSociale(String raisonSociale) {
        this.raisonSociale = raisonSociale;
    }

    public int getNoRue() {
        return noRue;
    }

    public void setNoRue(int noRue) {
        this.noRue = noRue;
    }

    public String getRue() {
        return rue;
    }

    public void setRue(String rue) {
        this.rue = rue;
    }

    public int getcPostal() {
        return cPostal;
    }

    public void setcPostal(int cPostal) {
        this.cPostal = cPostal;
    }

    public String getVille() {
        return ville;
    }

    public void setVille(String ville) {
        this.ville = ville;
    }

    public int getNoTel() {
        return noTel;
    }

    public void setNoTel(int noTel) {
        this.noTel = noTel;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }


}
