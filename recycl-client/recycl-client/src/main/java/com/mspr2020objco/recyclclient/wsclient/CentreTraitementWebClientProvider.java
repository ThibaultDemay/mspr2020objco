package com.mspr2020objco.recyclclient.wsclient;

import com.mspr2020objco.recyclclient.model.CentreTraitement;
import org.springframework.stereotype.Component;

@Component
public class CentreTraitementWebClientProvider extends WebClientProvider<CentreTraitement> {
    public CentreTraitementWebClientProvider() {
		super(CentreTraitement.class, "nocentre");
	}
}