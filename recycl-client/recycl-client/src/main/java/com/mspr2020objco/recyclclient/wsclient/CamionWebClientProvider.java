package com.mspr2020objco.recyclclient.wsclient;

import com.mspr2020objco.recyclclient.model.Camion;
import org.springframework.stereotype.Component;

@Component
public class CamionWebClientProvider extends WebClientProvider<Camion> {

    public CamionWebClientProvider() {
		super(Camion.class, "noimmatric");
	}
}