package com.mspr2020objco.recyclclient.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;

public class DetailDemande {

    @JsonProperty("quantiteenlevee")
    private int quantiteEnlevee;
    @JsonProperty("remarque")
    private String remarque;
    @JsonProperty("nodemande")
    private int noDemande;
    @JsonProperty("notypedechet")
    private int noTypeDechet;

    public DetailDemande(int quantiteEnlevee, String remarque, int noDemande, int noTypeDechet) {
        this.quantiteEnlevee = quantiteEnlevee;
        this.remarque = remarque;
        this.noDemande = noDemande;
        this.noTypeDechet = noTypeDechet;
    }

    public DetailDemande(){
        super();
    }

    public String getRemarque() {
        return remarque;
    }

    public void setRemarque(String remarque) {
        this.remarque = remarque;
    }

    public int getQuantiteEnlevee() {
        return quantiteEnlevee;
    }

    public void setQuantiteEnlevee(int quantiteEnlevee) {
        this.quantiteEnlevee = quantiteEnlevee;
    }

    public int getNoDemande() {
        return noDemande;
    }

    public void setNoDemande(int noDemande) {
        this.noDemande = noDemande;
    }

    public int getNoTypeDechet() {
        return noTypeDechet;
    }

    public void setNoTypeDechet(int noTypeDechet) {
        this.noTypeDechet = noTypeDechet;
    }
}
