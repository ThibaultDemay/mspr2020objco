package com.mspr2020objco.recyclclient.controller;

import com.mspr2020objco.recyclclient.model.CentreTraitement;
import com.mspr2020objco.recyclclient.model.TypeDechet;
import com.mspr2020objco.recyclclient.model.TypeDechetQuantite;
import com.mspr2020objco.recyclclient.service.CentreTraitementService;
import com.mspr2020objco.recyclclient.service.TypeDechetService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
public class TypeDechetController {

    @Autowired
    private TypeDechetService typeDechetService;

    @Autowired
    private CentreTraitementService centreTraitementService;

    @GetMapping("/types-dechets")
    public ModelAndView show(Model model) {
        List<TypeDechet> typeDechetList = this.typeDechetService.getAll();
        Map<String, Object> params = new HashMap<>();
        params.put("typesdechets", typeDechetList);
        return new ModelAndView("typesDechets", params);
    }

    @GetMapping("/types-dechets/{id}")
    public ModelAndView showTypeDechets(Model model, @PathVariable String id) {
        TypeDechet typeDechet = typeDechetService.getById(id);
        Map<String, Object> params = new HashMap<>();
        params.put("typeDechets", typeDechet);
        return new ModelAndView("typeDechets", params);
    }

    @GetMapping("/quantite-dechets/")
    public ModelAndView showTypeDechets(Model model, @RequestParam(name = "date1") String date1,
                                        @RequestParam(name = "date2") String date2) {
        List<TypeDechetQuantite> typdeDechetQuantiteList = typeDechetService.getTypeDechetQuantiteBtwDates(date1, date2);
        Map<String, Object> params = new HashMap<>();
        params.put("typesDechetsQuantite", typdeDechetQuantiteList);
        params.put("date1", date1);
        params.put("date2", date2);
        return new ModelAndView("typesDechetsQuantite", params);
    }



    @GetMapping("/quantite-dechets-centre")
    public ModelAndView showQuantiteDechetCentreForm(Model model) {
        List<TypeDechet> typeDechetList = typeDechetService.getAll();
        List<CentreTraitement> centreTraitementList = centreTraitementService.getAll();
        Map<String, Object> params = new HashMap<>();
        params.put("dechetsTypes", typeDechetList);
        params.put("centresTraitement", centreTraitementList);

        return new ModelAndView("quantiteDechetCentreForm", params);
    }

    @PostMapping("/quantite-dechets-centre")
    public ModelAndView submitQuantiteDechetCentreForm(Model model, @RequestParam(name = "date1") String date1,
                                                       @RequestParam(name = "date2") String date2,
                                                       @RequestParam(name = "nomTypeDechet", required = false) String nomTypeDechet,
                                                       @RequestParam(name = "noCentre", required = false) Integer noCentre,
                                                       @RequestParam(name = "checkAllCentres", required = false) boolean checkAllCentres,
                                                       @RequestParam(name = "checkAllTypesDechets", required = false) boolean checkAllTypesDechets) {
        List<TypeDechetQuantite> typdeDechetQuantiteList;
        Map<String, Object> params = new HashMap<>();
        // Recherche nationale sur tous les déchets
        if (checkAllCentres == true && checkAllTypesDechets == true) {
            typdeDechetQuantiteList = typeDechetService.getTypeDechetQuantiteBtwDates(date1, date2);
        }
        // Recherche nationale d'un type de déchet précis
        else if (checkAllCentres == true && checkAllTypesDechets == false) {
            typdeDechetQuantiteList = typeDechetService.getTypeDechetQuantiteTypeDechetBtwDates(nomTypeDechet, date1, date2);
        }
        // Recherche sur un centre précis de tous les déchets
        else if (checkAllCentres == false && checkAllTypesDechets == true) {
            typdeDechetQuantiteList = typeDechetService.getTypeDechetQuantiteByCentreBtwDates(noCentre, date1, date2);
            params.put("noCentre", noCentre);
        }
        // Recherche sur un centre précis d'un type de déchet précis
        else{
            typdeDechetQuantiteList = typeDechetService.getTypeDechetQuantiteByCentreTypeDechetBtwDates(noCentre, nomTypeDechet, date1, date2);
            params.put("noCentre", noCentre);
        }
        params.put("typesDechetsQuantite", typdeDechetQuantiteList);
        params.put("date1", date1);
        params.put("date2", date2);
        params.put("allCentres", checkAllCentres);
        params.put("allTypesDechets", checkAllTypesDechets);
        return new ModelAndView("typesDechetsQuantite", params);
    }


}
